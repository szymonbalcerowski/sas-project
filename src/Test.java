import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple tests
 * @author Szymon Balcerowski
 *
 */

public class Test {
    /**
     *  accuracy = 10000
     *  interval [2,5]
     * (x^2+2x)dx=60 
     */
	@org.junit.Test
	public void test() {
		Project project=new Project();
		String[] myStringArray = new String[4];
		myStringArray[0]="1";
		myStringArray[1]="2";
		myStringArray[2]="2";
		myStringArray[3]="1";
		List<Monomial> list=new ArrayList<Monomial>();
		project.listCreator(myStringArray, list);
		double integral=project.integral(2, 5, 10000, list);
		assertTrue(integral<60.5 && integral >59.5);
	}

	
	
	
	
	@org.junit.Test(expected=NumberFormatException.class)
	public void test2() {
		Project project=new Project();
		String[] myStringArray = new String[4];
		myStringArray[0]="2";
		myStringArray[1]="1";
		myStringArray[2]="error";
		myStringArray[3]="3";
		List<Monomial> list=new ArrayList<Monomial>();
		project.listCreator(myStringArray, list);
	}
}
