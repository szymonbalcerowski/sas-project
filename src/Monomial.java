/**
 * This class implements simple monomial 
 * @author Szymon Balcerowski
 *
 */
public class Monomial{
    private int degree;
    private double coefficient;
    Monomial(int deg,double coe)
    {
        setDegree(deg);
        setCoefficient(coe);
    }
    public int getDegree() 
    {
        return degree;
    }
    public void setDegree(int degree) 
    {
        this.degree = degree;
    }
    public double getCoefficient() 
    {
        return coefficient;
    }
    public void setCoefficient(double wsp) 
    {
        this.coefficient = wsp;
    }
}