import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**
 * This class implements Monte Carlo integration
 * @author Szymon Balcerowski
 * @version 1.0
 */
public class Project{
    
	/**
	 * 
	 * @param x random point belong to interval of integration
	 * @param m list of monomial
	 * @return value of polynomial for x 
	 */
	public static double monteCarloMethodOne(double x,List<Monomial> m) 
	{
        double suma=0;
        for(int i=0;i<m.size();i++)
        {
            suma+=Math.pow(x, m.get(i).getDegree())*m.get(i).getCoefficient();
        }
        return suma;
    }
    /**
     * Print polynomial
     * @param m list of monomial
     */
	public static void polynomial(List<Monomial> m){
	        System.out.print("("+m.get(0).getCoefficient()+")*x^"+m.get(0).getDegree());
	        for(int i=1;i<m.size();i++)
	        {
	            System.out.print("+("+m.get(i).getCoefficient()+")*x^"+m.get(i).getDegree());
	        }
	}
	/**
	 * 
	 * @param begin of interval
	 * @param end of interval
	 * @param accuaracy amount of random points
	 * @param m list of monomial
	 * @return approximate value of integral
	 */
	public double integral(double begin, double end, int accuaracy, List<Monomial> m){
		double integral=0;
        for (int i=0; i<accuaracy; i++) 
        {
            integral += monteCarloMethodOne(begin + Math.random() * (end-begin),m);
        }
      return  integral = (integral / (double)accuaracy) * (end - begin);
	}
	
	/**
	 * Creats list of monomial
	 * @param args command-line arguments
	 * @param list list of monomial
	 */
	public void listCreator(String[] args,List<Monomial> list){
	   
		for(int i=0;i<args.length;i+=2)
        {
            int degree=0;
            double coefficient=0;
            try 
            {
                degree = Integer.parseInt(args[i+1]);
                coefficient = Double.parseDouble(args[i]);
                if(degree<0)
                {
                    System.err.println("Invalid input parameter values.(degree of monomial must be non-negative integer)");
                    System.exit(1);
                }
                } catch (NumberFormatException e)
            {
                System.err.println("Invalid input parameter values.");
                System.exit(-1);
            }
            list.add(new Monomial(degree, coefficient));
        }
	}
	
	
	public static void main(String[] args)
	{
        Project project=new Project();
        double begin=0;
        double end=0;
        int accuaracy=0;
        List<Monomial> list=new ArrayList<Monomial>();
        
        if(args.length%2==0 && args.length!=0)
        {
        	project.listCreator(args, list);
        }
        else 
        {
            System.err.println("Invalid number of parameters");
            return;
        }
        
        System.out.println("Your polynomial:");
        polynomial(list);
        Scanner scaner=new Scanner(System.in);
        String s="";
        
        /**
         * take input from console
         */
        boolean error=true;
        while(error)
        {
        	 try 
             {
					System.out.println("\nEnter beginning of interval");
					s=scaner.nextLine();
					begin=Double.parseDouble(s);
					System.out.println("Enter ending of interval");
					s=scaner.nextLine();
					end=Double.parseDouble(s);
					System.out.println("Enter accuracy");
					s=scaner.nextLine();
					accuaracy=Integer.parseInt(s);
					if (accuaracy <= 0){
					    System.err.println("Invalid input. Accuracy must be positive integer");
					error=true;}
					else error=false;
				} catch (NumberFormatException e) 
             {
	                System.err.println("Invalid input. Please try again");
	                error=true;
				}
        }
        /**
         * Printing result
         */
        System.out.println("Interval of integration ["+begin+","+end+"]");
        System.out.print("Integral(");
        polynomial(list);
        System.out.print(")dx= ");
        System.out.println(project.integral(begin, end, accuaracy, list));
        System.out.println("Calculated with Monte Carlo method and accurancy "+accuaracy);
    }
}